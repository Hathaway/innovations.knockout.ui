import { ErrorSummary } from './ErrorSummary'

export interface IKoModalOptions {
    ElementID: string;
    BodyTemplate: string;
    BodyData: any;
    ModalTitle: string;
    ShowClose?: boolean;
    ShowSave?: boolean;
    ModalType?: string;
    CloseText?: string;
    SaveText?: string;
    SaveCallBack?: Function;
    CloseCallBack?: Function;
    Validate?: boolean;
    FormID?: string;
    ModalHeaderTemplate?: string;
    ErrorSummary?: ErrorSummary;
    ErrorIcon?: string;
}

export interface KoModalValidationError {
    [propname: string]: any
}

export class KoModal {

    public ErrorSummary: KnockoutObservable<ErrorSummary>;
    public ElementID: KnockoutObservable<string>;
    public BodyTemplate: KnockoutObservable<string>;
    public BodyData: KnockoutObservable<any>;
    public ShowClose: KnockoutObservable<boolean>;
    public ShowSave: KnockoutObservable<boolean>;
    public ModalType: KnockoutObservable<string>;
    public ModalTitle: KnockoutObservable<string>;
    public CloseText: KnockoutObservable<string>;
    public SaveText: KnockoutObservable<string>;
    public SaveCallBack: Function;
    public CloseCallBack: Function;
    public Validate: boolean;
    public Validator: JQueryValidation.Validator;
    public FormID: string;
    public ModalHeaderTemplate : KnockoutObservable<string>;
    public Options: IKoModalOptions;
    constructor(options: IKoModalOptions) {

        var self = this;
        this.Options = options;

        if (this.Options.ErrorIcon == null)
        {
            this.Options.ErrorIcon = "fa fa-exclamation-triangle";
        }

        this.ModalHeaderTemplate = ko.observable("modal-header-template");

        if (options.ErrorSummary) {
            this.ErrorSummary = ko.observable(options.ErrorSummary);
        } else {

            this.ErrorSummary = ko.observable(new ErrorSummary());
        }

        this.BodyTemplate = ko.observable(options.BodyTemplate);
        this.BodyData = ko.observable(options.BodyData);
        this.ModalTitle = ko.observable(options.ModalTitle);
        this.ElementID = ko.observable(options.ElementID);
        this.Validate = (options.Validate === null || options.Validate === undefined) ? true : options.Validate ;
        this.ShowClose = ko.observable(true);
        this.ShowSave = ko.observable(true);
        this.ModalType = ko.observable("");
        this.SaveText = ko.observable("Save Changes");
        this.CloseText = ko.observable("Close");


        this.ErrorSummary().Errors.subscribe((errors) =>
        {
            if (self.Validator) {

                //reset first
                self.Validator.showErrors(null);

                //show errors
                for (var e = 0; e < errors.length; e++)
                {
                    var error = errors[e];
                    var valerrors: KoModalValidationError = { };
                    var name = error.IsModel ? error.ModelName : error.Name;
                    valerrors[name] = error.Message;

                    var element = (self.Validator as any).findByName(name);
                    if (element.length > 0) {
                        //add error to validator
                        self.Validator.showErrors(valerrors);
                    }
                }

                self.ShowErrorsInTabs();
            }
        });

        if (options) {

            if (options.FormID !== undefined) {
                this.FormID = options.FormID;
            }

            if (options.ShowClose !== undefined) {
                this.ShowClose(options.ShowClose);
            }

            if (options.ShowSave !== undefined) {
                this.ShowSave(options.ShowSave);
            }

            if (options.ModalType !== undefined) {
                this.ModalType(options.ModalType);
            }

            if (options.SaveCallBack !== undefined) {
                this.SaveCallBack = options.SaveCallBack;
            }

            if (options.CloseCallBack !== undefined) {
                this.CloseCallBack = options.CloseCallBack;
            }

            if (options.SaveText !== undefined) {
                this.SaveText(options.SaveText);
            }

            if (options.CloseText !== undefined) {
                this.CloseText(options.CloseText);
            }

            if (options.ModalHeaderTemplate !== undefined) {
                this.ModalHeaderTemplate(options.ModalHeaderTemplate);
            }
        }
    }

    ShowErrorsInTabs = () =>
    {
        var shownFirstTabError = false;
        //find tabs and add in error icon

        for (var e = 0; e < this.Validator.errorList.length; e++) {
            var item = this.Validator.errorList[e];
            var tabPane = $(item.element).parents('.tab-pane');
            if (tabPane.length > 0) {

                var tabPaneId = tabPane.attr('id');

                var tabNav = $('a[href="#' + tabPaneId + '"][data-toggle="tab"]');

                if (!shownFirstTabError) {
                    tabNav.tab('show');
                }

                var error = tabNav.find(".tab-error");
                if (error.length == 0) {
                    tabNav.append("<span class='tab-error error "+ this.Options.ErrorIcon +"'></span>");
                }
            }
        }

    }

    Save = () => {

        var self = this;

        var modal = $("#" + this.ElementID());

        var formJquery : JQuery = null;

        if (self.FormID != null) {
            formJquery = modal.find("#" + self.FormID);
        } else {
            formJquery = modal.find("form");
        }

        if (this.Validate)
        {
            this.ErrorSummary().Errors([]);

            this.Validator = formJquery.validate({
                showErrors: null,
                ignore: "",
                invalidHandler: function (event, validator)
                {
                    self.ShowErrorsInTabs();
                },
                submitHandler: function (formElement)
                {
                    //remove error icons
                    $(formElement).find(".tab-error").remove();
                    if (self.SaveCallBack && self.BodyData()) {
                        self.SaveCallBack.call(self, self.BodyData(), modal, formJquery, formElement);
                    }
                },
                errorPlacement: function (errorLabel, element)
                {
                    var container = $(element).parents(".input-group");
                    if (container.length > 0) {
                        errorLabel.detach().insertAfter(container);
                    } else {
                        errorLabel.insertAfter(element);
                    }
                }
            });
            formJquery.submit();
        } else {

            if (self.SaveCallBack && self.BodyData()) {
                self.SaveCallBack.call(self, self.BodyData(), modal, formJquery, formJquery[0]);
            }
        }
    }

    Close = () => {
        if (this.CloseCallBack) {
            this.CloseCallBack.call(this);
        }
    }

    ShowModal () : JQuery {

        var modalJQuery = $("#" + this.ElementID());
        modalJQuery.modal("show");  
        return modalJQuery;
    }

    HideModal(): JQuery {

        var modalJQuery = $("#" + this.ElementID());
        modalJQuery.modal("hide");
        return modalJQuery;
    }
}

export class KoSingleValueForm {
    public Label: KnockoutObservable<string>;
    public Value: KnockoutObservable<string>;
    public Placeholder: KnockoutObservable<string>;
    constructor(value: string, label: string, placeholder?: string) {
        this.Value = ko.observable(value);
        this.Label = ko.observable(label);
        this.Placeholder = ko.observable(null);
        if (placeholder) {
            this.Placeholder = ko.observable(placeholder);
        }
    }
    public Reset() {
        this.Value("");
    }
}


export interface IKoMessageBoxOptions
{
    ElementID: string;

    Title: string;
    Text: string;

    CloseText?: string;
    OkText?: string;

    BodyTemplate?: string;
    BodyData?: any; 

    ShowOk?: boolean;
    ShowClose?: boolean;
    OkCallback?: Function;
    CloseCallback?: Function;
    ErrorSummary?: ErrorSummary;
}

export class KoMessageBox
{
    public ElementID: KnockoutObservable<string>;
    public Title: KnockoutObservable<string>;
    public Text: KnockoutObservable<string>;

    public BodyTemplate: KnockoutObservable<string>;
    public BodyData: KnockoutObservable<any>;

    public OkText: KnockoutObservable<string>;
    public CloseText: KnockoutObservable<string>;

    public ShowOk: KnockoutObservable<boolean>;
    public ShowClose: KnockoutObservable<boolean>;

    public OkCallback: Function;
    public CloseCallback: Function;

    public ErrorSummary: KnockoutObservable<ErrorSummary>;

    constructor(options: IKoMessageBoxOptions)
    {
        if (options.ErrorSummary) {
            this.ErrorSummary = ko.observable(options.ErrorSummary);
        } else {

            this.ErrorSummary = ko.observable(new ErrorSummary());
        }

        this.ElementID = ko.observable(options.ElementID);
        this.Title = ko.observable(options.Title);
        this.Text = ko.observable(options.Text);
        this.BodyTemplate = ko.observable("message-box-body-template");
        this.BodyData = ko.observable(null);
        this.OkText = ko.observable("Ok");
        this.CloseText = ko.observable("Close")
        this.ShowOk = ko.observable(true);
        this.ShowClose = ko.observable(true);
        this.OkCallback = () => { };
        this.CloseCallback = () => { };

        if (options.BodyTemplate!=null)
        {
            this.BodyTemplate(options.BodyTemplate);
        }

        if (options.BodyData != null) {
            this.BodyData(options.BodyData);
        }

        if (options.OkText != null) {
            this.OkText(options.OkText);
        }

        if (options.CloseText != null) {
            this.CloseText(options.CloseText);
        }

        if (options.ShowOk != null) {
            this.ShowOk(options.ShowOk);
        }

        if (options.ShowClose != null) {
            this.ShowClose(options.ShowClose);
        }

        if (options.OkCallback != null) {
            this.OkCallback = options.OkCallback;
        }

        if (options.CloseCallback != null) {
            this.CloseCallback = options.CloseCallback;
        }
    }

    Ok()
    {
        if (this.OkCallback)
        {
            this.OkCallback();
        }

        this.Hide();
    }

    Close()
    {
        if (this.CloseCallback) {
            this.CloseCallback();
        }

        this.Hide();
    }

    Show(): JQuery {

        var modalJQuery = $("#" + this.ElementID());
        modalJQuery.modal("show");
        return modalJQuery;
    }

    Hide(): JQuery {

        var modalJQuery = $("#" + this.ElementID());
        modalJQuery.modal("hide");
        return modalJQuery;
    }
}
