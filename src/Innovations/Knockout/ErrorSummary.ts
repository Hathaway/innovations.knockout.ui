export class ModelStateError {
    public Name: string;
    public Message: string;
    public IsModel: boolean;
    public ModelName: string;

    constructor(name: string, message: string)
    {
        this.IsModel = false;

        var searchText = "model.";

        var indexof = name.indexOf(searchText);

        if (indexof > - 1)
        {
            this.ModelName = name.substr(indexof + searchText.length, name.length - searchText.length);
            this.Name = "Validation Error";
            this.IsModel = true;
        }
        else
        {
            this.Name = name;
        }
        this.Message = message;
    }
}

export class ErrorSummary
{
    public Errors: KnockoutObservableArray<ModelStateError>;

    constructor() {

        this.Errors = ko.observableArray([]);
    }

    public SetErrorFromAjax(jqXHR: any, textStatus: any, errorThrown: any)
    {
        this.Errors.removeAll();

        if (jqXHR.responseJSON && Object.keys(jqXHR.responseJSON).length > 0) {
            this.SetModelStateErrors(jqXHR.responseJSON);
        } else {
            this.Errors.push(new ModelStateError("Exception", errorThrown));
        }
    }

    SetModelStateErrors(modelState: any) {
        this.Errors.removeAll();

        var valuetxt = "value.";

        for (var key in modelState) {

            if (key.lastIndexOf(valuetxt, 0) == 0)
            {
                //rename to remove
                var newKey = key.substring(valuetxt.length, key.length);
                modelState[newKey] = modelState[key];
                delete modelState[key];
            }
        }

        for (var key in modelState) {
            if (modelState.hasOwnProperty(key) && modelState[key]) {
                var errorMessage = "";
                for (var i = 0; i < modelState[key].length; i++) {
                    if (i != 0)
                    {
                        errorMessage += ", " 
                    }
                    else {

                        if (modelState[key][i].ErrorMessage) {
                            errorMessage += modelState[key][i].ErrorMessage as string;
                        }
                        else {
                            errorMessage += (modelState[key][i] as string);
                        }
                    }
                }

                var error = new ModelStateError(key, errorMessage);
                this.Errors.push(error);
            }
        }
    }

}