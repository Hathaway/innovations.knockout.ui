﻿    export interface TreeViewOptions {
        OnNodeLoadChildren: (node: TreeViewNode) => JQueryPromise<TreeViewNode[]>;
        OnNodeSelected: (node: TreeViewNode) => void;
        RootIcon: string;
        FolderOpenIcon: string;
        FolderCloseIcon: string;
        ExpandedIcon: string;
        UnExpandedIcon: string;
    }

    export class TreeView {
        public Selected: KnockoutObservable<TreeViewNode>;

        public Nodes: KnockoutObservableArray<TreeViewNode>;

        public OnNodeLoadChildren: (node: TreeViewNode) => JQueryPromise<TreeViewNode[]>;

        public OnNodeSelected: (node: TreeViewNode) => void;

        public TreeViewOptions: TreeViewOptions;

        constructor(options: TreeViewOptions) {
            this.OnNodeLoadChildren = options.OnNodeLoadChildren;

            this.OnNodeSelected = options.OnNodeSelected;

            this.Selected = ko.observable(null);

            this.Nodes = ko.observableArray([]);

            this.TreeViewOptions = options;
        }

        public SelectNode(node: TreeViewNode) {
            this.Selected(node);
            this.OnNodeSelected(node);
        }
    }

    export class TreeViewNode {
        public TreeView: TreeView;

        public Selected: KnockoutObservable<boolean>;

        public SelectedCss: KnockoutObservable<string>;

        public IsOpen: KnockoutObservable<boolean>;

        public IsSelected: KnockoutComputed<boolean>;

        public ExpanderClass: KnockoutComputed<string>;

        public ExpandCollapsedIcon: KnockoutComputed<boolean>;

        public IconClass: KnockoutComputed<string>;

        public Depth: KnockoutComputed<number>;

        public IsLeaf: KnockoutObservable<boolean>;

        public IsRoot: KnockoutComputed<boolean>;

        public AreChildrenLoaded: KnockoutObservable<boolean>;

        public Parent: KnockoutObservable<TreeViewNode>;

        public Children: KnockoutObservableArray<TreeViewNode>;

        public Text: KnockoutObservable<string>;

        public ID: KnockoutObservable<string>;

        public RootIcon: KnockoutObservable<string>;

        public OpenIcon: KnockoutObservable<string>;

        public ClosedIcon: KnockoutObservable<string>;

        constructor(treeView: TreeView, parent?: TreeViewNode) {
            var self = this;

            this.TreeView = treeView;

            this.AreChildrenLoaded = ko.observable(false);

            this.ID = ko.observable("");

            this.Text = ko.observable("");

            this.RootIcon = ko.observable(treeView.TreeViewOptions.RootIcon);

            this.OpenIcon = ko.observable(treeView.TreeViewOptions.FolderOpenIcon);

            this.ClosedIcon = ko.observable(treeView.TreeViewOptions.FolderCloseIcon);

            this.Parent = ko.observable(parent);

            this.Children = ko.observableArray([]);

            this.IsOpen = ko.observable(false);

            this.IsRoot = ko.computed(() => {
                if (self.Parent() == null) {
                    return true;
                }
            });

            this.IsLeaf = ko.computed(() => {


                if (self.AreChildrenLoaded() && self.Children().length == 0) {
                    return true;
                }
                return false;


            });

            this.ExpanderClass = ko.computed(() => {
                //would be nice to show a transition here...

                if (self.IsLeaf()) {
                    return "leaf"; //todo put spacer in here
                }

                if (self.IsOpen()) {
                    return self.TreeView.TreeViewOptions.ExpandedIcon;
                } else {
                    return self.TreeView.TreeViewOptions.UnExpandedIcon;
                }

            });

            this.IconClass = ko.computed(() => {

                if (self.IsRoot) {
                    return self.RootIcon();
                }

                if (self.IsOpen()) {
                    return self.OpenIcon();
                } else {
                    return self.ClosedIcon();
                }

            });

            this.IsSelected = ko.computed(() => {
                if (self.TreeView.Selected() && self.TreeView.Selected() == this) {
                    return true;
                } else {
                    return false;
                }
            });

            this.SelectedCss = ko.computed(() => {
                if (self.IsSelected()) {
                    return "selected";
                } else {
                    return "unselected";
                }
            });

            this.Depth = ko.computed(() => {
                var ancestors = self.GetAncestors();
                return ancestors.length;
            });
        }

        GetAncestors(): TreeViewNode[] {
            var result: TreeViewNode[] = [];

            var parent = this.Parent();

            if (parent != null) {
                result.push(parent);
                var parentParents = parent.GetAncestors();
                if (parentParents.length > 0) {

                    for (var i = 0; i < parentParents.length; i++) {
                        result.push(parentParents[i]);
                    }


                }
            }

            return result;
        }

        public Select = () => {
            this.TreeView.SelectNode(this);
        }

        public ExpandCollapse = () => {
            if (this.AreChildrenLoaded()) {
                this.ToggleOpenClose();
            } else {
                this.LoadChildren();
            }
        }

        public ToggleOpenClose() {
            if (this.IsOpen()) {
                this.IsOpen(false);
            } else {
                this.IsOpen(true);
            }

        }

        public LoadChildren = () => {

            var self = this;

            self.TreeView.OnNodeLoadChildren(self).done((children) => {
                self.Children.removeAll();
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    self.Children.push(child);
                    child.Parent(self);
                }
                self.AreChildrenLoaded(true);
                self.ToggleOpenClose();
            });
        };
    }
