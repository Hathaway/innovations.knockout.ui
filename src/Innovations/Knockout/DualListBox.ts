﻿export interface IDualListBoxOptions
{
    Values: IDualListBoxOption[];
    GetAllItemFunc?: (filter: string) => JQueryPromise<IDualListBoxOption[]>;
    FilterFunc?: (items: KoDualListBoxOption[], filter: string) => KoDualListBoxOption[];
}

export interface IDualListBoxOption {
    Selected: boolean;
    Value: string;
    Text: string;
}

export class KoDualListBoxOption implements IDualListBoxOption {

    Selected: boolean;
    Value: string;
    Text: string;
    Highlighted: KnockoutObservable<boolean>;
    DualListBox: KoAjaxDualListBox;

    constructor(option: IDualListBoxOption, dualListBox : KoAjaxDualListBox)
    {
        this.Highlighted = ko.observable(false);
        this.Value = option.Value;
        this.Text = option.Text;
        this.Selected = option.Selected;
        this.DualListBox = dualListBox;
    }

    OnClick()
    {
        this.Highlighted(true);
    }

    ToJs()
    {
        var data =
        {
            Value: this.Value,
            Text : this.Text
        };

        return data;
    }
}


export class KoAjaxDualListBox
{
    UnSelectedFilteredLabel: KnockoutObservable<string>;

    SelectedFilteredLabel: KnockoutObservable<string>;

    UnSelectedFilter: KnockoutObservable<string>;

    SelectedFilter: KnockoutObservable<string>;

    SelectedHighlighted: KnockoutObservableArray<any>;

    UnSelectedHighlighted: KnockoutObservableArray<any>;

    SelectedOptions: KnockoutObservableArray<KoDualListBoxOption>;

    SelectedOptionsFiltered: KnockoutObservableArray<KoDualListBoxOption>;

    IsSelectedOptionsFiltered: KnockoutObservable<boolean>;

    UnSelectedOptions: KnockoutObservableArray<KoDualListBoxOption>;

    UnSelectedOptionsFiltered: KnockoutObservableArray<KoDualListBoxOption>;

    CurrentGetAllItemRequest: JQueryPromise<IDualListBoxOption[]>;

    IsUnSelectedOptionsFiltered: KnockoutObservable<boolean>;

    GetAllItemFunc?: (filter: string) => JQueryPromise<IDualListBoxOption[]>;

    FilterFunc: (items: KoDualListBoxOption[], filter: string) => KoDualListBoxOption[];

    UnSelectedFilterApplied: boolean;

    constructor(options: IDualListBoxOptions)
    {
        var self = this;

        this.FilterFunc = (items: KoDualListBoxOption[], filter : string) =>
        {
            var regEx = new RegExp(filter,'gi');
            var filtered = items.filter((value, index, array) =>
            {
                if (value.Text.match(regEx)) {
                    return true;
                } else {
                    return false;
                }
            });
            return filtered;
        };

        this.CurrentGetAllItemRequest = null;
        this.SelectedHighlighted = ko.observableArray([]);
        this.UnSelectedHighlighted = ko.observableArray([]);
        this.UnSelectedFilterApplied = false;
        this.SelectedOptions = ko.observableArray([]);
        this.UnSelectedOptions = ko.observableArray([]);
        this.SelectedFilter = ko.observable("");
        this.UnSelectedFilter = ko.observable("");
        this.SelectedOptionsFiltered = ko.observableArray([]);
        this.UnSelectedOptionsFiltered = ko.observableArray([]);

        this.IsSelectedOptionsFiltered = ko.observable(false);
        this.IsUnSelectedOptionsFiltered = ko.observable(false);
        this.UnSelectedFilteredLabel = ko.observable("");
        this.SelectedFilteredLabel = ko.observable("");

        this.SelectedFilter.subscribe((val: string) => {
            self.ApplySelectedFilter();
        });

        this.UnSelectedFilter.subscribe((val: string) => {
            self.ApplyUnSelectedFilter();
        });

        if (options.GetAllItemFunc)
        {
            this.GetAllItemFunc = options.GetAllItemFunc;
        }
        else
        {
            //set default
            this.GetAllItemFunc = (filter: string) => {
                var dfd = jQuery.Deferred();
                var values = self.UnSelectedOptions().map((value, index, arr) => {
                    return value as IDualListBoxOption;
                });
                dfd.resolve(values);
                return dfd.promise();
            };

        }

        this.SetData(options.Values);

    }

    SetSelected(data: IDualListBoxOption[])
    {

    }

    GetSelected()
    {
        var selected = this.SelectedOptions().map((val, index, arr) => {

            return val.ToJs();

        });

        return selected;
    }

    OnUnSelectedFilterKeyPress()
    {
        this.ApplyUnSelectedFilter();
    }

    OnSelectedFilterKeyPress()
    {
        this.ApplySelectedFilter();
    }

    SetData(options: IDualListBoxOption[])
    {
        var self = this;
        var selected : KoDualListBoxOption[] = [];
        var unselected: KoDualListBoxOption[] = [];

        //set initalvalues
        options.forEach((option, index, arr) => {
            if (option.Selected) {
                selected.push(new KoDualListBoxOption(option, self));
            } else {
                unselected.push(new KoDualListBoxOption(option, self));
            }
        });
        this.SelectedOptions(selected);
        this.UnSelectedOptions(unselected);
        this.ApplySelectedFilter();
        this.ApplyUnSelectedFilter();
    }

    ApplySelectedFilter()
    {
        this.ApplyFilter(this.SelectedOptions, this.SelectedFilter, this.SelectedOptionsFiltered, this.IsSelectedOptionsFiltered);
    }

    ApplyUnSelectedFilter()
    {
            //run get all item func
       var self = this;

       this.CurrentGetAllItemRequest = this.GetAllItemFunc(this.UnSelectedFilter())
            .done((data) => {

                var selected = self.SelectedOptions();
                var unselectedOptions : KoDualListBoxOption []= [];

                //remove selected items
                data.forEach((option, index, arr) => {
                    var found = false;
                    for (var selectedIndex = 0; selectedIndex < selected.length; selectedIndex++) {
                        var selectedOp = selected[selectedIndex];
                        if (option.Value == selectedOp.Value) {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        unselectedOptions.push(new KoDualListBoxOption(option, self));
                    }
                });

                //set unselected
                this.UnSelectedOptions(unselectedOptions);

                //apply filter
                this.ApplyFilter(this.UnSelectedOptions, this.UnSelectedFilter, this.UnSelectedOptionsFiltered, this.IsUnSelectedOptionsFiltered);

                this.UnSelectedFilterApplied = true;
            });
       

    }

    ApplyFilter(currentList: KnockoutObservableArray<KoDualListBoxOption>, filterTextKo: KnockoutObservable<string>, filteredListKo: KnockoutObservableArray<KoDualListBoxOption>, isSelectedKo : KnockoutObservable<boolean>)
    {
        var filterText = filterTextKo();
        if (filterText && filterText.length > 0)
        {
            var filtered = this.FilterFunc(currentList(), filterText);
            filteredListKo(filtered);
            isSelectedKo(true);
        }
        else
        {
            filteredListKo(currentList());
            isSelectedKo(false);
        }
    }

    ClearUnSelectedFilter() {
        this.UnSelectedFilter("");
    }

    ClearSelectedFilter() {
        this.SelectedFilter("");
    }

    SelectOptions()
    {
        var selectedValues = this.UnSelectedHighlighted();
        var filtered = this.UnSelectedOptionsFiltered();
        var selected = [];

        for (var s = 0; s < selectedValues.length; s++)
        {
            for (var f = 0; f < filtered.length; f++)
            {
                var fItem = filtered[f];
                if (selectedValues[s] == fItem.Value)
                {
                    this.SelectedOptions.push(fItem);
                    this.UnSelectedOptions.remove(fItem);
                    break;
                }
            }

        }

        this.ApplySelectedFilter();
        this.ApplyUnSelectedFilter();
        
    }

    RemoveOptions()
    {
        var selectedValues = this.SelectedHighlighted();
        var filtered = this.SelectedOptionsFiltered();
        var selected = [];

        for (var s = 0; s < selectedValues.length; s++) {
            for (var f = 0; f < filtered.length; f++) {
                var fItem = filtered[f];
                if (selectedValues[s] == fItem.Value) {
                    this.UnSelectedOptions.push(fItem);
                    this.SelectedOptions.remove(fItem);
                    break;
                }
            }

        }

        this.ApplySelectedFilter();
        this.ApplyUnSelectedFilter();
    }

    RemoveAll() {
        var self = this;
        //move all unselected into selected (filtered only)
        var all = this.SelectedOptions();
        var filtered = this.SelectedOptionsFiltered();
        var current = this.UnSelectedOptions();

        var toKeep = [];

        for (var a = 0; a < all.length; a++) {
            var found = false;
            for (var f = 0; f < filtered.length; f++) {
                if (all[a].Value == filtered[f].Value)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                toKeep.push(all[a]);
            }
        }
        this.SelectedOptions(toKeep);

        var all = filtered.concat(current);
        this.UnSelectedOptions(all);

        
       

        this.ApplySelectedFilter();
        this.ApplyUnSelectedFilter();
    }

    SelectAll() {
        var self = this;
        //move all unselected into selected (filtered only)
        var filtered = this.UnSelectedOptionsFiltered();
        var current = this.SelectedOptions();
        var all = filtered.concat(current);
        this.SelectedOptions(all);

        filtered.forEach((val, ind, arr) => {
            self.UnSelectedOptions.remove(val);
        });

        this.ApplySelectedFilter();
        this.ApplyUnSelectedFilter();
    }
 
   
}

