import { ErrorSummary } from './ErrorSummary'

export class AjaxHelper {
    public Url: string;

    public ErrorSummary: KnockoutObservable<ErrorSummary>;

    public Loading: KnockoutObservable<boolean>;

    constructor(url: string, errorSummary?: KnockoutObservable<ErrorSummary>) {
        this.Url = url;
        if (errorSummary) {
            this.ErrorSummary = errorSummary;
        } else {
            this.ErrorSummary = ko.observable(new ErrorSummary());
        }
        this.Loading = ko.observable(false);
    }

    public PostAction(data: any, action: string): JQueryPromise<any>
    {
        var url = this.Url + "/" + action;

        return this.Ajax(url, 'POST', data);
    }

    public Post(data : any): JQueryPromise<any> {
        return this.Ajax(this.Url, 'POST', data);
    }

    public Put(id : any, data: any): JQueryPromise<any> {

        var url = this.Url + "/" + id;

        if (id == null) {
            url = this.Url;
        }

        return this.Ajax(url, 'PUT', data);
    }

    public Delete(id : any): JQueryPromise<any> {
        var url = this.Url + "/" + id;
        return this.Ajax(url, 'DELETE', null);
    }

    public Get(): JQueryPromise<any> {
        return this.Ajax(this.Url, 'GET');
    }


    public GetWithData(data?: any): JQueryPromise<any> {
        return this.Ajax(this.Url, 'GET', data);
    }

    public GetWithId(id : any, data?: any): JQueryPromise<any> {
        var url = this.Url + "/" + id;
        if (data) {
            url += "?" + $.param(data);
        }
        return this.Ajax(url, 'GET', null);
    }

    Ajax(url: string, method: string, data?: any): JQueryPromise<any> {
        var self = this;

        var options : JQueryAjaxSettings = {
            type: method,
            url: url,
            dataType: null, // Set to null (was 'json') dur to issue with PUT try to parse JSON
            contentType: 'application/json',
            data: data,
            cache: false
        };

        if (data) {
            data = this.DeleteConstructors(data);
        }

        if (data && (method == 'POST' || method == "PUT")) {
            options.data = JSON.stringify(data);
        }

        //Due to issue with PUT try to parse JSON
        if (data && (method == 'POST' || method == "GET")) {
            options.dataType = 'json';
        }


        self.Loading(true);

        return $.ajax(options)
            .done(() => {
                self.Loading(false);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                self.Loading(false);
                self.ErrorSummary().SetErrorFromAjax(jqXHR, textStatus, errorThrown);
            });
    }

    DeleteConstructors(data?: any): any {

        if (data && data.constructor) {
            delete data.constructor
        }

        if (data && $.isArray(data)) {
            for (var i = 0; i < data.length; i++) {
                var dataItem = data[i];
                if (dataItem.constructor) {
                    delete dataItem.constructor
                    data[i] = dataItem;
                }
            }
        }

        return data;
    }

}

export class AjaxQueryHelper extends AjaxHelper
{

    constructor(url: string, errorSummary?: KnockoutObservable<ErrorSummary>)
    {
        super(url, errorSummary);

    }

    public ListAll(): JQueryPromise<any> {
        var url = this.Url + "/ListAll";
        return this.Ajax(url, 'GET');
    }


    public Query(data: any): JQueryPromise<any> {
        var url = this.Url + "/Query";
        return this.Ajax(url, 'GET', data);
    }


    public Single(id : any): JQueryPromise<any> {
        var url = this.Url + "/Single/" + id;
        return this.Ajax(url, 'GET');
    }

}