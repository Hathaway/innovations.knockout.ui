var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
System.register("Innovations/Knockout/ErrorSummary", [], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ModelStateError, ErrorSummary;
    return {
        setters: [],
        execute: function () {
            ModelStateError = (function () {
                function ModelStateError(name, message) {
                    this.IsModel = false;
                    var searchText = "model.";
                    var indexof = name.indexOf(searchText);
                    if (indexof > -1) {
                        this.ModelName = name.substr(indexof + searchText.length, name.length - searchText.length);
                        this.Name = "Validation Error";
                        this.IsModel = true;
                    }
                    else {
                        this.Name = name;
                    }
                    this.Message = message;
                }
                return ModelStateError;
            }());
            exports_1("ModelStateError", ModelStateError);
            ErrorSummary = (function () {
                function ErrorSummary() {
                    this.Errors = ko.observableArray([]);
                }
                ErrorSummary.prototype.SetErrorFromAjax = function (jqXHR, textStatus, errorThrown) {
                    this.Errors.removeAll();
                    if (jqXHR.responseJSON && Object.keys(jqXHR.responseJSON).length > 0) {
                        this.SetModelStateErrors(jqXHR.responseJSON);
                    }
                    else {
                        this.Errors.push(new ModelStateError("Exception", errorThrown));
                    }
                };
                ErrorSummary.prototype.SetModelStateErrors = function (modelState) {
                    this.Errors.removeAll();
                    var valuetxt = "value.";
                    for (var key in modelState) {
                        if (key.lastIndexOf(valuetxt, 0) == 0) {
                            var newKey = key.substring(valuetxt.length, key.length);
                            modelState[newKey] = modelState[key];
                            delete modelState[key];
                        }
                    }
                    for (var key in modelState) {
                        if (modelState.hasOwnProperty(key) && modelState[key]) {
                            var errorMessage = "";
                            for (var i = 0; i < modelState[key].length; i++) {
                                if (i != 0) {
                                    errorMessage += ", ";
                                }
                                else {
                                    if (modelState[key][i].ErrorMessage) {
                                        errorMessage += modelState[key][i].ErrorMessage;
                                    }
                                    else {
                                        errorMessage += modelState[key][i];
                                    }
                                }
                            }
                            var error = new ModelStateError(key, errorMessage);
                            this.Errors.push(error);
                        }
                    }
                };
                return ErrorSummary;
            }());
            exports_1("ErrorSummary", ErrorSummary);
        }
    };
});
System.register("Innovations/Knockout/AjaxHelper", ["Innovations/Knockout/ErrorSummary"], function (exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    var ErrorSummary_1, AjaxHelper, AjaxQueryHelper;
    return {
        setters: [
            function (ErrorSummary_1_1) {
                ErrorSummary_1 = ErrorSummary_1_1;
            }
        ],
        execute: function () {
            AjaxHelper = (function () {
                function AjaxHelper(url, errorSummary) {
                    this.Url = url;
                    if (errorSummary) {
                        this.ErrorSummary = errorSummary;
                    }
                    else {
                        this.ErrorSummary = ko.observable(new ErrorSummary_1.ErrorSummary());
                    }
                    this.Loading = ko.observable(false);
                }
                AjaxHelper.prototype.PostAction = function (data, action) {
                    var url = this.Url + "/" + action;
                    return this.Ajax(url, 'POST', data);
                };
                AjaxHelper.prototype.Post = function (data) {
                    return this.Ajax(this.Url, 'POST', data);
                };
                AjaxHelper.prototype.Put = function (id, data) {
                    var url = this.Url + "/" + id;
                    if (id == null) {
                        url = this.Url;
                    }
                    return this.Ajax(url, 'PUT', data);
                };
                AjaxHelper.prototype.Delete = function (id) {
                    var url = this.Url + "/" + id;
                    return this.Ajax(url, 'DELETE', null);
                };
                AjaxHelper.prototype.Get = function () {
                    return this.Ajax(this.Url, 'GET');
                };
                AjaxHelper.prototype.GetWithData = function (data) {
                    return this.Ajax(this.Url, 'GET', data);
                };
                AjaxHelper.prototype.GetWithId = function (id, data) {
                    var url = this.Url + "/" + id;
                    if (data) {
                        url += "?" + $.param(data);
                    }
                    return this.Ajax(url, 'GET', null);
                };
                AjaxHelper.prototype.Ajax = function (url, method, data) {
                    var self = this;
                    var options = {
                        type: method,
                        url: url,
                        dataType: null,
                        contentType: 'application/json',
                        data: data,
                        cache: false
                    };
                    if (data) {
                        data = this.DeleteConstructors(data);
                    }
                    if (data && (method == 'POST' || method == "PUT")) {
                        options.data = JSON.stringify(data);
                    }
                    if (data && (method == 'POST' || method == "GET")) {
                        options.dataType = 'json';
                    }
                    self.Loading(true);
                    return $.ajax(options)
                        .done(function () {
                        self.Loading(false);
                    })
                        .fail(function (jqXHR, textStatus, errorThrown) {
                        self.Loading(false);
                        self.ErrorSummary().SetErrorFromAjax(jqXHR, textStatus, errorThrown);
                    });
                };
                AjaxHelper.prototype.DeleteConstructors = function (data) {
                    if (data && data.constructor) {
                        delete data.constructor;
                    }
                    if (data && $.isArray(data)) {
                        for (var i = 0; i < data.length; i++) {
                            var dataItem = data[i];
                            if (dataItem.constructor) {
                                delete dataItem.constructor;
                                data[i] = dataItem;
                            }
                        }
                    }
                    return data;
                };
                return AjaxHelper;
            }());
            exports_2("AjaxHelper", AjaxHelper);
            AjaxQueryHelper = (function (_super) {
                __extends(AjaxQueryHelper, _super);
                function AjaxQueryHelper(url, errorSummary) {
                    return _super.call(this, url, errorSummary) || this;
                }
                AjaxQueryHelper.prototype.ListAll = function () {
                    var url = this.Url + "/ListAll";
                    return this.Ajax(url, 'GET');
                };
                AjaxQueryHelper.prototype.Query = function (data) {
                    var url = this.Url + "/Query";
                    return this.Ajax(url, 'GET', data);
                };
                AjaxQueryHelper.prototype.Single = function (id) {
                    var url = this.Url + "/Single/" + id;
                    return this.Ajax(url, 'GET');
                };
                return AjaxQueryHelper;
            }(AjaxHelper));
            exports_2("AjaxQueryHelper", AjaxQueryHelper);
        }
    };
});
System.register("Innovations/Knockout/DualListBox", [], function (exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    var KoDualListBoxOption, KoAjaxDualListBox;
    return {
        setters: [],
        execute: function () {
            KoDualListBoxOption = (function () {
                function KoDualListBoxOption(option, dualListBox) {
                    this.Highlighted = ko.observable(false);
                    this.Value = option.Value;
                    this.Text = option.Text;
                    this.Selected = option.Selected;
                    this.DualListBox = dualListBox;
                }
                KoDualListBoxOption.prototype.OnClick = function () {
                    this.Highlighted(true);
                };
                KoDualListBoxOption.prototype.ToJs = function () {
                    var data = {
                        Value: this.Value,
                        Text: this.Text
                    };
                    return data;
                };
                return KoDualListBoxOption;
            }());
            exports_3("KoDualListBoxOption", KoDualListBoxOption);
            KoAjaxDualListBox = (function () {
                function KoAjaxDualListBox(options) {
                    var self = this;
                    this.FilterFunc = function (items, filter) {
                        var regEx = new RegExp(filter, 'gi');
                        var filtered = items.filter(function (value, index, array) {
                            if (value.Text.match(regEx)) {
                                return true;
                            }
                            else {
                                return false;
                            }
                        });
                        return filtered;
                    };
                    this.CurrentGetAllItemRequest = null;
                    this.SelectedHighlighted = ko.observableArray([]);
                    this.UnSelectedHighlighted = ko.observableArray([]);
                    this.UnSelectedFilterApplied = false;
                    this.SelectedOptions = ko.observableArray([]);
                    this.UnSelectedOptions = ko.observableArray([]);
                    this.SelectedFilter = ko.observable("");
                    this.UnSelectedFilter = ko.observable("");
                    this.SelectedOptionsFiltered = ko.observableArray([]);
                    this.UnSelectedOptionsFiltered = ko.observableArray([]);
                    this.IsSelectedOptionsFiltered = ko.observable(false);
                    this.IsUnSelectedOptionsFiltered = ko.observable(false);
                    this.UnSelectedFilteredLabel = ko.observable("");
                    this.SelectedFilteredLabel = ko.observable("");
                    this.SelectedFilter.subscribe(function (val) {
                        self.ApplySelectedFilter();
                    });
                    this.UnSelectedFilter.subscribe(function (val) {
                        self.ApplyUnSelectedFilter();
                    });
                    if (options.GetAllItemFunc) {
                        this.GetAllItemFunc = options.GetAllItemFunc;
                    }
                    else {
                        this.GetAllItemFunc = function (filter) {
                            var dfd = jQuery.Deferred();
                            var values = self.UnSelectedOptions().map(function (value, index, arr) {
                                return value;
                            });
                            dfd.resolve(values);
                            return dfd.promise();
                        };
                    }
                    this.SetData(options.Values);
                }
                KoAjaxDualListBox.prototype.SetSelected = function (data) {
                };
                KoAjaxDualListBox.prototype.GetSelected = function () {
                    var selected = this.SelectedOptions().map(function (val, index, arr) {
                        return val.ToJs();
                    });
                    return selected;
                };
                KoAjaxDualListBox.prototype.OnUnSelectedFilterKeyPress = function () {
                    this.ApplyUnSelectedFilter();
                };
                KoAjaxDualListBox.prototype.OnSelectedFilterKeyPress = function () {
                    this.ApplySelectedFilter();
                };
                KoAjaxDualListBox.prototype.SetData = function (options) {
                    var self = this;
                    var selected = [];
                    var unselected = [];
                    options.forEach(function (option, index, arr) {
                        if (option.Selected) {
                            selected.push(new KoDualListBoxOption(option, self));
                        }
                        else {
                            unselected.push(new KoDualListBoxOption(option, self));
                        }
                    });
                    this.SelectedOptions(selected);
                    this.UnSelectedOptions(unselected);
                    this.ApplySelectedFilter();
                    this.ApplyUnSelectedFilter();
                };
                KoAjaxDualListBox.prototype.ApplySelectedFilter = function () {
                    this.ApplyFilter(this.SelectedOptions, this.SelectedFilter, this.SelectedOptionsFiltered, this.IsSelectedOptionsFiltered);
                };
                KoAjaxDualListBox.prototype.ApplyUnSelectedFilter = function () {
                    var _this = this;
                    var self = this;
                    this.CurrentGetAllItemRequest = this.GetAllItemFunc(this.UnSelectedFilter())
                        .done(function (data) {
                        var selected = self.SelectedOptions();
                        var unselectedOptions = [];
                        data.forEach(function (option, index, arr) {
                            var found = false;
                            for (var selectedIndex = 0; selectedIndex < selected.length; selectedIndex++) {
                                var selectedOp = selected[selectedIndex];
                                if (option.Value == selectedOp.Value) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                unselectedOptions.push(new KoDualListBoxOption(option, self));
                            }
                        });
                        _this.UnSelectedOptions(unselectedOptions);
                        _this.ApplyFilter(_this.UnSelectedOptions, _this.UnSelectedFilter, _this.UnSelectedOptionsFiltered, _this.IsUnSelectedOptionsFiltered);
                        _this.UnSelectedFilterApplied = true;
                    });
                };
                KoAjaxDualListBox.prototype.ApplyFilter = function (currentList, filterTextKo, filteredListKo, isSelectedKo) {
                    var filterText = filterTextKo();
                    if (filterText && filterText.length > 0) {
                        var filtered = this.FilterFunc(currentList(), filterText);
                        filteredListKo(filtered);
                        isSelectedKo(true);
                    }
                    else {
                        filteredListKo(currentList());
                        isSelectedKo(false);
                    }
                };
                KoAjaxDualListBox.prototype.ClearUnSelectedFilter = function () {
                    this.UnSelectedFilter("");
                };
                KoAjaxDualListBox.prototype.ClearSelectedFilter = function () {
                    this.SelectedFilter("");
                };
                KoAjaxDualListBox.prototype.SelectOptions = function () {
                    var selectedValues = this.UnSelectedHighlighted();
                    var filtered = this.UnSelectedOptionsFiltered();
                    var selected = [];
                    for (var s = 0; s < selectedValues.length; s++) {
                        for (var f = 0; f < filtered.length; f++) {
                            var fItem = filtered[f];
                            if (selectedValues[s] == fItem.Value) {
                                this.SelectedOptions.push(fItem);
                                this.UnSelectedOptions.remove(fItem);
                                break;
                            }
                        }
                    }
                    this.ApplySelectedFilter();
                    this.ApplyUnSelectedFilter();
                };
                KoAjaxDualListBox.prototype.RemoveOptions = function () {
                    var selectedValues = this.SelectedHighlighted();
                    var filtered = this.SelectedOptionsFiltered();
                    var selected = [];
                    for (var s = 0; s < selectedValues.length; s++) {
                        for (var f = 0; f < filtered.length; f++) {
                            var fItem = filtered[f];
                            if (selectedValues[s] == fItem.Value) {
                                this.UnSelectedOptions.push(fItem);
                                this.SelectedOptions.remove(fItem);
                                break;
                            }
                        }
                    }
                    this.ApplySelectedFilter();
                    this.ApplyUnSelectedFilter();
                };
                KoAjaxDualListBox.prototype.RemoveAll = function () {
                    var self = this;
                    var all = this.SelectedOptions();
                    var filtered = this.SelectedOptionsFiltered();
                    var current = this.UnSelectedOptions();
                    var toKeep = [];
                    for (var a = 0; a < all.length; a++) {
                        var found = false;
                        for (var f = 0; f < filtered.length; f++) {
                            if (all[a].Value == filtered[f].Value) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            toKeep.push(all[a]);
                        }
                    }
                    this.SelectedOptions(toKeep);
                    var all = filtered.concat(current);
                    this.UnSelectedOptions(all);
                    this.ApplySelectedFilter();
                    this.ApplyUnSelectedFilter();
                };
                KoAjaxDualListBox.prototype.SelectAll = function () {
                    var self = this;
                    var filtered = this.UnSelectedOptionsFiltered();
                    var current = this.SelectedOptions();
                    var all = filtered.concat(current);
                    this.SelectedOptions(all);
                    filtered.forEach(function (val, ind, arr) {
                        self.UnSelectedOptions.remove(val);
                    });
                    this.ApplySelectedFilter();
                    this.ApplyUnSelectedFilter();
                };
                return KoAjaxDualListBox;
            }());
            exports_3("KoAjaxDualListBox", KoAjaxDualListBox);
        }
    };
});
System.register("Innovations/Knockout/KoModal", ["Innovations/Knockout/ErrorSummary"], function (exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    var ErrorSummary_2, KoModal, KoSingleValueForm, KoMessageBox;
    return {
        setters: [
            function (ErrorSummary_2_1) {
                ErrorSummary_2 = ErrorSummary_2_1;
            }
        ],
        execute: function () {
            KoModal = (function () {
                function KoModal(options) {
                    var _this = this;
                    this.ShowErrorsInTabs = function () {
                        var shownFirstTabError = false;
                        for (var e = 0; e < _this.Validator.errorList.length; e++) {
                            var item = _this.Validator.errorList[e];
                            var tabPane = $(item.element).parents('.tab-pane');
                            if (tabPane.length > 0) {
                                var tabPaneId = tabPane.attr('id');
                                var tabNav = $('a[href="#' + tabPaneId + '"][data-toggle="tab"]');
                                if (!shownFirstTabError) {
                                    tabNav.tab('show');
                                }
                                var error = tabNav.find(".tab-error");
                                if (error.length == 0) {
                                    tabNav.append("<span class='tab-error error " + _this.Options.ErrorIcon + "'></span>");
                                }
                            }
                        }
                    };
                    this.Save = function () {
                        var self = _this;
                        var modal = $("#" + _this.ElementID());
                        var formJquery = null;
                        if (self.FormID != null) {
                            formJquery = modal.find("#" + self.FormID);
                        }
                        else {
                            formJquery = modal.find("form");
                        }
                        if (_this.Validate) {
                            _this.ErrorSummary().Errors([]);
                            _this.Validator = formJquery.validate({
                                showErrors: null,
                                ignore: "",
                                invalidHandler: function (event, validator) {
                                    self.ShowErrorsInTabs();
                                },
                                submitHandler: function (formElement) {
                                    $(formElement).find(".tab-error").remove();
                                    if (self.SaveCallBack && self.BodyData()) {
                                        self.SaveCallBack.call(self, self.BodyData(), modal, formJquery, formElement);
                                    }
                                },
                                errorPlacement: function (errorLabel, element) {
                                    var container = $(element).parents(".input-group");
                                    if (container.length > 0) {
                                        errorLabel.detach().insertAfter(container);
                                    }
                                    else {
                                        errorLabel.insertAfter(element);
                                    }
                                }
                            });
                            formJquery.submit();
                        }
                        else {
                            if (self.SaveCallBack && self.BodyData()) {
                                self.SaveCallBack.call(self, self.BodyData(), modal, formJquery, formJquery[0]);
                            }
                        }
                    };
                    this.Close = function () {
                        if (_this.CloseCallBack) {
                            _this.CloseCallBack.call(_this);
                        }
                    };
                    var self = this;
                    this.Options = options;
                    if (this.Options.ErrorIcon == null) {
                        this.Options.ErrorIcon = "fa fa-exclamation-triangle";
                    }
                    this.ModalHeaderTemplate = ko.observable("modal-header-template");
                    if (options.ErrorSummary) {
                        this.ErrorSummary = ko.observable(options.ErrorSummary);
                    }
                    else {
                        this.ErrorSummary = ko.observable(new ErrorSummary_2.ErrorSummary());
                    }
                    this.BodyTemplate = ko.observable(options.BodyTemplate);
                    this.BodyData = ko.observable(options.BodyData);
                    this.ModalTitle = ko.observable(options.ModalTitle);
                    this.ElementID = ko.observable(options.ElementID);
                    this.Validate = (options.Validate === null || options.Validate === undefined) ? true : options.Validate;
                    this.ShowClose = ko.observable(true);
                    this.ShowSave = ko.observable(true);
                    this.ModalType = ko.observable("");
                    this.SaveText = ko.observable("Save Changes");
                    this.CloseText = ko.observable("Close");
                    this.ErrorSummary().Errors.subscribe(function (errors) {
                        if (self.Validator) {
                            self.Validator.showErrors(null);
                            for (var e = 0; e < errors.length; e++) {
                                var error = errors[e];
                                var valerrors = {};
                                var name = error.IsModel ? error.ModelName : error.Name;
                                valerrors[name] = error.Message;
                                var element = self.Validator.findByName(name);
                                if (element.length > 0) {
                                    self.Validator.showErrors(valerrors);
                                }
                            }
                            self.ShowErrorsInTabs();
                        }
                    });
                    if (options) {
                        if (options.FormID !== undefined) {
                            this.FormID = options.FormID;
                        }
                        if (options.ShowClose !== undefined) {
                            this.ShowClose(options.ShowClose);
                        }
                        if (options.ShowSave !== undefined) {
                            this.ShowSave(options.ShowSave);
                        }
                        if (options.ModalType !== undefined) {
                            this.ModalType(options.ModalType);
                        }
                        if (options.SaveCallBack !== undefined) {
                            this.SaveCallBack = options.SaveCallBack;
                        }
                        if (options.CloseCallBack !== undefined) {
                            this.CloseCallBack = options.CloseCallBack;
                        }
                        if (options.SaveText !== undefined) {
                            this.SaveText(options.SaveText);
                        }
                        if (options.CloseText !== undefined) {
                            this.CloseText(options.CloseText);
                        }
                        if (options.ModalHeaderTemplate !== undefined) {
                            this.ModalHeaderTemplate(options.ModalHeaderTemplate);
                        }
                    }
                }
                KoModal.prototype.ShowModal = function () {
                    var modalJQuery = $("#" + this.ElementID());
                    modalJQuery.modal("show");
                    return modalJQuery;
                };
                KoModal.prototype.HideModal = function () {
                    var modalJQuery = $("#" + this.ElementID());
                    modalJQuery.modal("hide");
                    return modalJQuery;
                };
                return KoModal;
            }());
            exports_4("KoModal", KoModal);
            KoSingleValueForm = (function () {
                function KoSingleValueForm(value, label, placeholder) {
                    this.Value = ko.observable(value);
                    this.Label = ko.observable(label);
                    this.Placeholder = ko.observable(null);
                    if (placeholder) {
                        this.Placeholder = ko.observable(placeholder);
                    }
                }
                KoSingleValueForm.prototype.Reset = function () {
                    this.Value("");
                };
                return KoSingleValueForm;
            }());
            exports_4("KoSingleValueForm", KoSingleValueForm);
            KoMessageBox = (function () {
                function KoMessageBox(options) {
                    if (options.ErrorSummary) {
                        this.ErrorSummary = ko.observable(options.ErrorSummary);
                    }
                    else {
                        this.ErrorSummary = ko.observable(new ErrorSummary_2.ErrorSummary());
                    }
                    this.ElementID = ko.observable(options.ElementID);
                    this.Title = ko.observable(options.Title);
                    this.Text = ko.observable(options.Text);
                    this.BodyTemplate = ko.observable("message-box-body-template");
                    this.BodyData = ko.observable(null);
                    this.OkText = ko.observable("Ok");
                    this.CloseText = ko.observable("Close");
                    this.ShowOk = ko.observable(true);
                    this.ShowClose = ko.observable(true);
                    this.OkCallback = function () { };
                    this.CloseCallback = function () { };
                    if (options.BodyTemplate != null) {
                        this.BodyTemplate(options.BodyTemplate);
                    }
                    if (options.BodyData != null) {
                        this.BodyData(options.BodyData);
                    }
                    if (options.OkText != null) {
                        this.OkText(options.OkText);
                    }
                    if (options.CloseText != null) {
                        this.CloseText(options.CloseText);
                    }
                    if (options.ShowOk != null) {
                        this.ShowOk(options.ShowOk);
                    }
                    if (options.ShowClose != null) {
                        this.ShowClose(options.ShowClose);
                    }
                    if (options.OkCallback != null) {
                        this.OkCallback = options.OkCallback;
                    }
                    if (options.CloseCallback != null) {
                        this.CloseCallback = options.CloseCallback;
                    }
                }
                KoMessageBox.prototype.Ok = function () {
                    if (this.OkCallback) {
                        this.OkCallback();
                    }
                    this.Hide();
                };
                KoMessageBox.prototype.Close = function () {
                    if (this.CloseCallback) {
                        this.CloseCallback();
                    }
                    this.Hide();
                };
                KoMessageBox.prototype.Show = function () {
                    var modalJQuery = $("#" + this.ElementID());
                    modalJQuery.modal("show");
                    return modalJQuery;
                };
                KoMessageBox.prototype.Hide = function () {
                    var modalJQuery = $("#" + this.ElementID());
                    modalJQuery.modal("hide");
                    return modalJQuery;
                };
                return KoMessageBox;
            }());
            exports_4("KoMessageBox", KoMessageBox);
        }
    };
});
System.register("Innovations/Knockout/Treeview", [], function (exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    var TreeView, TreeViewNode;
    return {
        setters: [],
        execute: function () {
            TreeView = (function () {
                function TreeView(options) {
                    this.OnNodeLoadChildren = options.OnNodeLoadChildren;
                    this.OnNodeSelected = options.OnNodeSelected;
                    this.Selected = ko.observable(null);
                    this.Nodes = ko.observableArray([]);
                    this.TreeViewOptions = options;
                }
                TreeView.prototype.SelectNode = function (node) {
                    this.Selected(node);
                    this.OnNodeSelected(node);
                };
                return TreeView;
            }());
            exports_5("TreeView", TreeView);
            TreeViewNode = (function () {
                function TreeViewNode(treeView, parent) {
                    var _this = this;
                    this.Select = function () {
                        _this.TreeView.SelectNode(_this);
                    };
                    this.ExpandCollapse = function () {
                        if (_this.AreChildrenLoaded()) {
                            _this.ToggleOpenClose();
                        }
                        else {
                            _this.LoadChildren();
                        }
                    };
                    this.LoadChildren = function () {
                        var self = _this;
                        self.TreeView.OnNodeLoadChildren(self).done(function (children) {
                            self.Children.removeAll();
                            for (var i = 0; i < children.length; i++) {
                                var child = children[i];
                                self.Children.push(child);
                                child.Parent(self);
                            }
                            self.AreChildrenLoaded(true);
                            self.ToggleOpenClose();
                        });
                    };
                    var self = this;
                    this.TreeView = treeView;
                    this.AreChildrenLoaded = ko.observable(false);
                    this.ID = ko.observable("");
                    this.Text = ko.observable("");
                    this.RootIcon = ko.observable(treeView.TreeViewOptions.RootIcon);
                    this.OpenIcon = ko.observable(treeView.TreeViewOptions.FolderOpenIcon);
                    this.ClosedIcon = ko.observable(treeView.TreeViewOptions.FolderCloseIcon);
                    this.Parent = ko.observable(parent);
                    this.Children = ko.observableArray([]);
                    this.IsOpen = ko.observable(false);
                    this.IsRoot = ko.computed(function () {
                        if (self.Parent() == null) {
                            return true;
                        }
                    });
                    this.IsLeaf = ko.computed(function () {
                        if (self.AreChildrenLoaded() && self.Children().length == 0) {
                            return true;
                        }
                        return false;
                    });
                    this.ExpanderClass = ko.computed(function () {
                        if (self.IsLeaf()) {
                            return "leaf";
                        }
                        if (self.IsOpen()) {
                            return self.TreeView.TreeViewOptions.ExpandedIcon;
                        }
                        else {
                            return self.TreeView.TreeViewOptions.UnExpandedIcon;
                        }
                    });
                    this.IconClass = ko.computed(function () {
                        if (self.IsRoot) {
                            return self.RootIcon();
                        }
                        if (self.IsOpen()) {
                            return self.OpenIcon();
                        }
                        else {
                            return self.ClosedIcon();
                        }
                    });
                    this.IsSelected = ko.computed(function () {
                        if (self.TreeView.Selected() && self.TreeView.Selected() == _this) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    });
                    this.SelectedCss = ko.computed(function () {
                        if (self.IsSelected()) {
                            return "selected";
                        }
                        else {
                            return "unselected";
                        }
                    });
                    this.Depth = ko.computed(function () {
                        var ancestors = self.GetAncestors();
                        return ancestors.length;
                    });
                }
                TreeViewNode.prototype.GetAncestors = function () {
                    var result = [];
                    var parent = this.Parent();
                    if (parent != null) {
                        result.push(parent);
                        var parentParents = parent.GetAncestors();
                        if (parentParents.length > 0) {
                            for (var i = 0; i < parentParents.length; i++) {
                                result.push(parentParents[i]);
                            }
                        }
                    }
                    return result;
                };
                TreeViewNode.prototype.ToggleOpenClose = function () {
                    if (this.IsOpen()) {
                        this.IsOpen(false);
                    }
                    else {
                        this.IsOpen(true);
                    }
                };
                return TreeViewNode;
            }());
            exports_5("TreeViewNode", TreeViewNode);
        }
    };
});
//# sourceMappingURL=innovations-knockout-ui.js.map