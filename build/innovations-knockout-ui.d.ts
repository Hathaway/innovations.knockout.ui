declare module "Innovations/Knockout/ErrorSummary" {
    export class ModelStateError {
        Name: string;
        Message: string;
        IsModel: boolean;
        ModelName: string;
        constructor(name: string, message: string);
    }
    export class ErrorSummary {
        Errors: KnockoutObservableArray<ModelStateError>;
        constructor();
        SetErrorFromAjax(jqXHR: any, textStatus: any, errorThrown: any): void;
        SetModelStateErrors(modelState: any): void;
    }
}
declare module "Innovations/Knockout/AjaxHelper" {
    import { ErrorSummary } from "Innovations/Knockout/ErrorSummary";
    export class AjaxHelper {
        Url: string;
        ErrorSummary: KnockoutObservable<ErrorSummary>;
        Loading: KnockoutObservable<boolean>;
        constructor(url: string, errorSummary?: KnockoutObservable<ErrorSummary>);
        PostAction(data: any, action: string): JQueryPromise<any>;
        Post(data: any): JQueryPromise<any>;
        Put(id: any, data: any): JQueryPromise<any>;
        Delete(id: any): JQueryPromise<any>;
        Get(): JQueryPromise<any>;
        GetWithData(data?: any): JQueryPromise<any>;
        GetWithId(id: any, data?: any): JQueryPromise<any>;
        Ajax(url: string, method: string, data?: any): JQueryPromise<any>;
        DeleteConstructors(data?: any): any;
    }
    export class AjaxQueryHelper extends AjaxHelper {
        constructor(url: string, errorSummary?: KnockoutObservable<ErrorSummary>);
        ListAll(): JQueryPromise<any>;
        Query(data: any): JQueryPromise<any>;
        Single(id: any): JQueryPromise<any>;
    }
}
declare module "Innovations/Knockout/DualListBox" {
    export interface IDualListBoxOptions {
        Values: IDualListBoxOption[];
        GetAllItemFunc?: (filter: string) => JQueryPromise<IDualListBoxOption[]>;
        FilterFunc?: (items: KoDualListBoxOption[], filter: string) => KoDualListBoxOption[];
    }
    export interface IDualListBoxOption {
        Selected: boolean;
        Value: string;
        Text: string;
    }
    export class KoDualListBoxOption implements IDualListBoxOption {
        Selected: boolean;
        Value: string;
        Text: string;
        Highlighted: KnockoutObservable<boolean>;
        DualListBox: KoAjaxDualListBox;
        constructor(option: IDualListBoxOption, dualListBox: KoAjaxDualListBox);
        OnClick(): void;
        ToJs(): {
            Value: string;
            Text: string;
        };
    }
    export class KoAjaxDualListBox {
        UnSelectedFilteredLabel: KnockoutObservable<string>;
        SelectedFilteredLabel: KnockoutObservable<string>;
        UnSelectedFilter: KnockoutObservable<string>;
        SelectedFilter: KnockoutObservable<string>;
        SelectedHighlighted: KnockoutObservableArray<any>;
        UnSelectedHighlighted: KnockoutObservableArray<any>;
        SelectedOptions: KnockoutObservableArray<KoDualListBoxOption>;
        SelectedOptionsFiltered: KnockoutObservableArray<KoDualListBoxOption>;
        IsSelectedOptionsFiltered: KnockoutObservable<boolean>;
        UnSelectedOptions: KnockoutObservableArray<KoDualListBoxOption>;
        UnSelectedOptionsFiltered: KnockoutObservableArray<KoDualListBoxOption>;
        CurrentGetAllItemRequest: JQueryPromise<IDualListBoxOption[]>;
        IsUnSelectedOptionsFiltered: KnockoutObservable<boolean>;
        GetAllItemFunc?: (filter: string) => JQueryPromise<IDualListBoxOption[]>;
        FilterFunc: (items: KoDualListBoxOption[], filter: string) => KoDualListBoxOption[];
        UnSelectedFilterApplied: boolean;
        constructor(options: IDualListBoxOptions);
        SetSelected(data: IDualListBoxOption[]): void;
        GetSelected(): {
            Value: string;
            Text: string;
        }[];
        OnUnSelectedFilterKeyPress(): void;
        OnSelectedFilterKeyPress(): void;
        SetData(options: IDualListBoxOption[]): void;
        ApplySelectedFilter(): void;
        ApplyUnSelectedFilter(): void;
        ApplyFilter(currentList: KnockoutObservableArray<KoDualListBoxOption>, filterTextKo: KnockoutObservable<string>, filteredListKo: KnockoutObservableArray<KoDualListBoxOption>, isSelectedKo: KnockoutObservable<boolean>): void;
        ClearUnSelectedFilter(): void;
        ClearSelectedFilter(): void;
        SelectOptions(): void;
        RemoveOptions(): void;
        RemoveAll(): void;
        SelectAll(): void;
    }
}
declare module "Innovations/Knockout/KoModal" {
    import { ErrorSummary } from "Innovations/Knockout/ErrorSummary";
    export interface IKoModalOptions {
        ElementID: string;
        BodyTemplate: string;
        BodyData: any;
        ModalTitle: string;
        ShowClose?: boolean;
        ShowSave?: boolean;
        ModalType?: string;
        CloseText?: string;
        SaveText?: string;
        SaveCallBack?: Function;
        CloseCallBack?: Function;
        Validate?: boolean;
        FormID?: string;
        ModalHeaderTemplate?: string;
        ErrorSummary?: ErrorSummary;
        ErrorIcon?: string;
    }
    export interface KoModalValidationError {
        [propname: string]: any;
    }
    export class KoModal {
        ErrorSummary: KnockoutObservable<ErrorSummary>;
        ElementID: KnockoutObservable<string>;
        BodyTemplate: KnockoutObservable<string>;
        BodyData: KnockoutObservable<any>;
        ShowClose: KnockoutObservable<boolean>;
        ShowSave: KnockoutObservable<boolean>;
        ModalType: KnockoutObservable<string>;
        ModalTitle: KnockoutObservable<string>;
        CloseText: KnockoutObservable<string>;
        SaveText: KnockoutObservable<string>;
        SaveCallBack: Function;
        CloseCallBack: Function;
        Validate: boolean;
        Validator: JQueryValidation.Validator;
        FormID: string;
        ModalHeaderTemplate: KnockoutObservable<string>;
        Options: IKoModalOptions;
        constructor(options: IKoModalOptions);
        ShowErrorsInTabs: () => void;
        Save: () => void;
        Close: () => void;
        ShowModal(): JQuery;
        HideModal(): JQuery;
    }
    export class KoSingleValueForm {
        Label: KnockoutObservable<string>;
        Value: KnockoutObservable<string>;
        Placeholder: KnockoutObservable<string>;
        constructor(value: string, label: string, placeholder?: string);
        Reset(): void;
    }
    export interface IKoMessageBoxOptions {
        ElementID: string;
        Title: string;
        Text: string;
        CloseText?: string;
        OkText?: string;
        BodyTemplate?: string;
        BodyData?: any;
        ShowOk?: boolean;
        ShowClose?: boolean;
        OkCallback?: Function;
        CloseCallback?: Function;
        ErrorSummary?: ErrorSummary;
    }
    export class KoMessageBox {
        ElementID: KnockoutObservable<string>;
        Title: KnockoutObservable<string>;
        Text: KnockoutObservable<string>;
        BodyTemplate: KnockoutObservable<string>;
        BodyData: KnockoutObservable<any>;
        OkText: KnockoutObservable<string>;
        CloseText: KnockoutObservable<string>;
        ShowOk: KnockoutObservable<boolean>;
        ShowClose: KnockoutObservable<boolean>;
        OkCallback: Function;
        CloseCallback: Function;
        ErrorSummary: KnockoutObservable<ErrorSummary>;
        constructor(options: IKoMessageBoxOptions);
        Ok(): void;
        Close(): void;
        Show(): JQuery;
        Hide(): JQuery;
    }
}
declare module "Innovations/Knockout/Treeview" {
    export interface TreeViewOptions {
        OnNodeLoadChildren: (node: TreeViewNode) => JQueryPromise<TreeViewNode[]>;
        OnNodeSelected: (node: TreeViewNode) => void;
        RootIcon: string;
        FolderOpenIcon: string;
        FolderCloseIcon: string;
        ExpandedIcon: string;
        UnExpandedIcon: string;
    }
    export class TreeView {
        Selected: KnockoutObservable<TreeViewNode>;
        Nodes: KnockoutObservableArray<TreeViewNode>;
        OnNodeLoadChildren: (node: TreeViewNode) => JQueryPromise<TreeViewNode[]>;
        OnNodeSelected: (node: TreeViewNode) => void;
        TreeViewOptions: TreeViewOptions;
        constructor(options: TreeViewOptions);
        SelectNode(node: TreeViewNode): void;
    }
    export class TreeViewNode {
        TreeView: TreeView;
        Selected: KnockoutObservable<boolean>;
        SelectedCss: KnockoutObservable<string>;
        IsOpen: KnockoutObservable<boolean>;
        IsSelected: KnockoutComputed<boolean>;
        ExpanderClass: KnockoutComputed<string>;
        ExpandCollapsedIcon: KnockoutComputed<boolean>;
        IconClass: KnockoutComputed<string>;
        Depth: KnockoutComputed<number>;
        IsLeaf: KnockoutObservable<boolean>;
        IsRoot: KnockoutComputed<boolean>;
        AreChildrenLoaded: KnockoutObservable<boolean>;
        Parent: KnockoutObservable<TreeViewNode>;
        Children: KnockoutObservableArray<TreeViewNode>;
        Text: KnockoutObservable<string>;
        ID: KnockoutObservable<string>;
        RootIcon: KnockoutObservable<string>;
        OpenIcon: KnockoutObservable<string>;
        ClosedIcon: KnockoutObservable<string>;
        constructor(treeView: TreeView, parent?: TreeViewNode);
        GetAncestors(): TreeViewNode[];
        Select: () => void;
        ExpandCollapse: () => void;
        ToggleOpenClose(): void;
        LoadChildren: () => void;
    }
}
