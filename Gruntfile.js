﻿/// <binding BeforeBuild='default' />
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks("grunt-ts");
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.initConfig({
        clean:['build'],
        concat: {
            options: { stripBanners: true },
            dist : {
                src: ['src/**/*.html'], dest: 'dist/Templates/innovations-knockout-ui.html'
            }
        },
        ts: {
            default: {
                tsconfig:true
            }
        },
        copy:
        {
            main:
            {
                files: [
                    { expand: true, cwd: 'build/', src: '*.js', dest: 'dist/', flatten: true, filter: 'isFile' },
                    { expand: true, cwd: 'build/', src: '*.d.ts', dest: 'dist/typings/', flatten: true, filter: 'isFile' }
                ]
            }
        }
        
    });

    grunt.registerTask('default', ['clean','concat','ts','copy']);
};